import tkinter as tk
import tkinter.filedialog
import csv
import pandas as pd


class Process:
    """
    The is the main class for calculating the score
    Please note, hit and run scoring is deactivated
    """

    def __init__(self, exported_path):
        self.exported_path = exported_path
        self.country_score = self.get_score('country')
        self.device_score = self.get_score('device')
        self.browser_score = self.get_score('browser')
        self.recording_table = pd.read_csv(exported_path)

        self.COUNTRY_FACTOR = 1
        self.DEVICE_FACTOR = 1
        self.BROWSER_FACTOR = 1

        self.set_weights()

    @staticmethod
    def get_score(type) -> dict:
        """
        This function gets the score from the setup .csvs.
        The default values is as of 2022-01-01
        :param type:
        :return:
        """
        if type == 'country':
            path = r'visitors.csv'
            def_val = {'US': 19.98, 'IN': 4.12, 'CA': 3.69, 'AU': 3.3, 'DE': 3.25, 'ZA': 2.38, 'SG': 2.27, 'NL': 2.11,
                       'MY': 2.08, 'IE': 1.87, 'FR': 1.53, 'NG': 1.5, 'AE': 1.45, 'CZ': 1.34, 'ES': 1.34, 'IT': 1.3,
                       'RO': 1.29, 'PT': 1.22, 'DZ': 1.16, 'CH': 1.13, 'BE': 1.07}
        elif type == 'device':
            path = r'devices.csv'
            def_val = {'Phone': 54.33, 'Desktop': 43.88, 'Tablet': 1.8}
        elif type == 'browser':
            path = r'browsers.csv'
            def_val = {'Chrome': 60.89, 'Safari': 27.97, 'Edge': 3.86, 'Firefox': 3.24, 'Samsung': 2.13}
        try:
            with open(path, mode='r') as csv_file:
                reader = csv.reader(csv_file, delimiter=',')
                result_dict = {rows[0]: float(rows[1]) for rows in reader}
        except FileNotFoundError:
            print(f'File not found in {path}. Using default values: {def_val}')
            result_dict = def_val
        return result_dict

    def set_weights(self):
        path = r'weights.csv'
        try:
            with open(path, mode='r') as csv_file:
                reader = csv.reader(csv_file, delimiter=',')
                result_dict = {rows[0]: float(rows[1]) for rows in reader}
                try:
                    self.COUNTRY_FACTOR = result_dict['Country']
                    self.DEVICE_FACTOR = result_dict['Device']
                    self.BROWSER_FACTOR = result_dict['Browser']
                except KeyError:
                    print(f'Invalid format in {path}. Using default values of 1 for all of the factors')
        except FileNotFoundError:
            print(f'File not found in {path}. Using default values of 1 for all of the factors')


    @staticmethod
    def get_result_path(path):
        return path.rsplit('.', 1)[0] + '_results.csv'

    def score_entries(self):

        def score_by_country(country):
            try:
                result = self.country_score[country]
            except KeyError:
                result = 1
            return result

        def score_by_device(device):
            try:
                result = self.device_score[device]
            except KeyError:
                result = 1
            return result

        def score_by_browser(browser):
            try:
                result = self.browser_score[browser]
            except KeyError:
                result = 1
            return result

        self.recording_table['Country Score'] = self.recording_table['Country'].apply(score_by_country)
        self.recording_table['Country Factor'] = self.COUNTRY_FACTOR

        self.recording_table['Device Score'] = self.recording_table['Device'].apply(score_by_device)
        self.recording_table['Device Factor'] = self.DEVICE_FACTOR

        self.recording_table['Browser Score'] = self.recording_table['Browser'].apply(score_by_browser)
        self.recording_table['Browser Factor'] = self.BROWSER_FACTOR

        # hit and run is dectivated
        # self.recording_table['Hit and Run Score'] = 1
        # self.recording_table['Hit and Run Score'][(self.recording_table['Landing Page URL'] == self.recording_table['Exit Page URL'])
        #                                           & (self.recording_table['Duration (seconds)'] < 60)] = 0

        self.recording_table['Score'] = self.recording_table['Country Score'] * self.recording_table['Country Factor'] \
                                        + self.recording_table['Device Score'] * self.recording_table['Device Factor'] \
                                        + self.recording_table['Browser Score'] * self.recording_table['Browser Factor']

        print(self.recording_table.head(10))

    def save_results(self):
        result_path = self.get_result_path(self.exported_path)
        self.recording_table.to_csv(result_path, index=False)


class MainWindow(tk.Frame):
    """
    This class responsible for displaying the GUI
    """

    def __init__(self, parent, *args, **kwargs):
        self.export_path = ""

        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.parent.title("Score Hotjar Recording")
        self.parent.geometry("300x150")
        tk.Label(self.parent, text="Please choose a file to analyse", font=('Georgia 13')).pack(pady=10)
        tk.Button(self.parent, text="Browse", command=self.open_file_and_score).pack(pady=10)
        self.wait_label = tk.Label(self.parent, text="", font=('Georgia 13'))
        self.wait_label.place(relx=0.5, rely=0.70, anchor='center')

    def open_file_and_score(self):
        self.wait_label.config(text='Analysing, please wait..')
        self.export_path = tk.filedialog.askopenfilenames(title='Choose a file',
                                                          filetypes=[('CSV Files', '*.csv .xls. xlsx')])[0]
        score_obj = Process(self.export_path)
        score_obj.score_entries()
        score_obj.save_results()
        self.wait_label.config(text='')
        result_path = Process.get_result_path(self.export_path)
        tk.messagebox.showinfo("Finished", f"Scoring is finished.\nThe result is saved to {result_path}")


def main():
    root = tk.Tk()
    MainWindow(root)
    root.mainloop()

    # Test
    # export_path = r'C:\Users\Nemeskee\Documents\BrokerChooser\hotjar\data\\recording_export_519586_2546458_2021-12-14T13-41-08 - Copy.csv'
    # main_stuff = Process(export_path)
    # main_stuff.score_entries()
    # main_stuff.save_results()


if __name__ == '__main__':
    main()
